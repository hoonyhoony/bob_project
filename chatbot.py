# -*- coding: utf-8 -*-
import re
import urllib.request
from bs4 import BeautifulSoup
from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
from string import punctuation
from menu import menu_daejeon
from menu import menu_seoul
from menu import menu_gwangju
from menu import menu_gumi
from operator import eq
import time

#-------------------------------
SLACK_TOKEN = ?
SLACK_SIGNING_SECRET = ?
#-------------------------------

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

# 밥 프로젝트 시작
def _start_bob(text):

    # year, month, day 값을 얻기 위해 time 라이브러리 사용
    now = time.localtime() #now는 타임 local값

    year = str(now.tm_year)

    if now.tm_mon < 10:
        month = str("0") + str(now.tm_mon) #month의 경우 1월부터 9월은 앞에 0을 붙이기위해 비교
    else:
        month = str(now.tm_mon)
    day = str(now.tm_mday) 

    menu_data = [] #menu.py 데이터에 들어있는 값을 $을 기준으로 split 하여 그것을 토대로 append시키는 공간
    real_data = [] #append 된 설정값을 한줄씩 출력하기 위한 공간

    attachment_list = [] #메인메뉴관련 리스트

    text = text.split("> ")[1] #봇에게 멘션을 이용한 명령어 입력시 따라오는 사용자 값을 잘라줌

    if "대전" == text: #대전
        real_data.append('*' + month + "월 " + day + "일 " + "대전 지역 점심 메뉴" + '*')
        for date, main_menu, menu in menu_daejeon: # from menu.py import menu_daejeon
            if date[0:2] == month and date[3:5] == day: # date[0:1] = 월, date[3:5] = 일
                menu_data.append(menu.split('$')) # $을 기준으로 나누어놓은 데이터 읽어들임
                search_menu = main_menu # 메인 메뉴 사진
        for menu in menu_data:
            for i in menu:
                real_data.append(i)
        

        query_text = urllib.parse.quote_plus(search_menu, encoding="utf-8") # 메인 메뉴 검색시 네이버의 경우 utf-8 형식으로 인코딩
        
        url = "https://search.naver.com/search.naver?where=image&sm=tab_jum&query=" + query_text
        sourcecode = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(sourcecode, "html.parser")
        
        first_step = soup.find("div", class_="img_area _item")
        second_step = first_step.find("img", class_="_img")['data-source']
        
        attachment = [{"text": "오늘의 메인메뉴 : " + search_menu, "image_url": second_step}]
        attachment_list.append(attachment)

        
        return u'\n'.join(real_data), attachment_list

    elif "서울" == text: #서울
        real_data.append('*' + month + "월 " + day + "일 " + "서울 지역 점심 메뉴" + '*')
        for date, main_menu, menu in menu_seoul:
            if date[0:2] == month and date[3:5] == day:
                menu_data.append(menu.split('$'))
                search_menu = main_menu
        for menu in menu_data:
            for i in menu:
                real_data.append(i)

        query_text = urllib.parse.quote_plus(search_menu, encoding="utf-8")
        
        url = "https://search.naver.com/search.naver?where=image&sm=tab_jum&query=" + query_text
        sourcecode = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(sourcecode, "html.parser")
        
        first_step = soup.find("div", class_="img_area _item")
        second_step = first_step.find("img", class_="_img")['data-source']
        
        attachment = [{"text": "오늘의 메인메뉴 : " + search_menu, "image_url": second_step}]

        attachment_list.append(attachment)

        return u'\n'.join(real_data), attachment_list

    elif "광주" == text: #광주
        real_data.append('*' + month + "월 " + day + "일 " + "광주 지역 점심 메뉴" + '*')
        for date, main_menu, menu in menu_gwangju:
            if date[0:2] == month and date[3:5] == day:
                menu_data.append(menu.split('$'))
                search_menu = main_menu
        for menu in menu_data:
            for i in menu:
                real_data.append(i)

        query_text = urllib.parse.quote_plus(search_menu, encoding="utf-8")
        
        url = "https://search.naver.com/search.naver?where=image&sm=tab_jum&query=" + query_text
        sourcecode = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(sourcecode, "html.parser")
        
        first_step = soup.find("div", class_="img_area _item")
        second_step = first_step.find("img", class_="_img")['data-source']
        
        attachment = [{"text": "오늘의 메인메뉴 : " + search_menu, "image_url": second_step}]

        attachment_list.append(attachment)

        return u'\n'.join(real_data), attachment_list

    elif "구미" == text: #구미
        real_data.append('*' +month + "월 " + day + "일 " + "구미 지역 점심 메뉴" + '*')
        for date, main_menu, menu in menu_gumi:
            if date[0:2] == month and date[3:5] == day:
                menu_data.append(menu.split('$'))
                search_menu = main_menu
        for menu in menu_data:
            for i in menu:
                real_data.append(i)

        query_text = urllib.parse.quote_plus(search_menu, encoding="utf-8")
        
        url = "https://search.naver.com/search.naver?where=image&sm=tab_jum&query=" + query_text
        sourcecode = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(sourcecode, "html.parser")
        
        first_step = soup.find("div", class_="img_area _item")
        second_step = first_step.find("img", class_="_img")['data-source']
        
        attachment = [{"text": "오늘의 메인메뉴 : " + search_menu, "image_url": second_step}]

        attachment_list.append(attachment)

        return u'\n'.join(real_data), attachment_list

    else:
        query_text = urllib.parse.quote_plus("404 not found", encoding="utf-8") #예외처리
        url = "https://search.naver.com/search.naver?where=image&sm=tab_jum&query=" + query_text
        sourcecode = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(sourcecode, "html.parser")
        
        first_step = soup.find("div", class_="img_area _item")
        second_step = first_step.find("img", class_="_img")['data-source']
        
        attachment = [{"text": "Error", "image_url": second_step}]
        attachment_list.append(attachment)
        
        return "서울, 대전, 구미, 광주 에서만 검색하세요.", attachment_list

    

# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    message, attachment = _start_bob(text)

    slack_web_client.chat_postMessage(
        channel=channel,
        text=message,
        attachments=attachment[0]
    )

    


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>BOB is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=8080)